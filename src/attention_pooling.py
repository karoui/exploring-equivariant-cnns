import torch.nn as nn

class AttentionPooling(nn.Module):
    def __init__(self, in_channels):
        super(AttentionPooling, self).__init__()
        self.conv = nn.Conv2d(in_channels, 1, kernel_size=1)
        self.softmax = nn.Softmax(dim=2)

    def forward(self, x):
        b, c, h, w = x.size()
        attention_scores = self.conv(x)
        attention_scores = attention_scores.view(b, 1, h * w)
        attention_scores = self.softmax(attention_scores)
        attention_scores = attention_scores.view(b, 1, h, w)

        weighted_features = (x * attention_scores).sum(dim=(2, 3))
        return weighted_features
