import torch
import torch.nn as nn
import torch.nn.functional as F


class LaplacianEncodingLayer(nn.Module):
    def __init__(self, out_channels):
        super(LaplacianEncodingLayer, self).__init__()
        self.laplacian_filter = torch.Tensor([[[[0, 1, 0], [1, -4, 1], [0, 1, 0]]]])
        self.out_channels = out_channels

    def forward(self, x):
        laplacian_filter = self.laplacian_filter.repeat(self.out_channels, 1, 1, 1).to(x.device)
        laplacian_encoding = F.conv2d(x, laplacian_filter, groups=x.size(1), padding=1)
        return laplacian_encoding