import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.optim.lr_scheduler import ReduceLROnPlateau

class BasicBlock(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(out_channels, out_channels, kernel_size=3, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(out_channels)
        self.downsample = downsample

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out


class ResNetEncoder(nn.Module):
    def __init__(self, block, desired_layers, num_classes, use_global_pooling=True, learning_rate=0.001, epochs=20, patience_stop=6, weight_decay=1e-4, max_grad_norm=None, fourier_encoder_cls=None, laplacian_encoder_cls=None, positional_encoding_cls=None, attention_pooling_cls=None):
        super(ResNetEncoder, self).__init__()
        self.block = block
        self.num_classes = num_classes
        self.use_global_pooling = use_global_pooling

        self.learning_rate = learning_rate
        self.epochs = epochs
        self.patience_stop = patience_stop
        self.weight_decay = weight_decay
        self.max_grad_norm = max_grad_norm

        self.fourier_encoder_cls = fourier_encoder_cls
        self.laplacian_encoder_cls = laplacian_encoder_cls
        self.positional_encoding_cls = positional_encoding_cls
        self.attention_pooling_cls = attention_pooling_cls

        self.in_channels = 64
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        self.layers = nn.ModuleList()
        for i, layer_blocks in enumerate(desired_layers):
            stride = 2 if i > 0 else 1
            res_block = self.make_layer(self.block, 64 * (2 ** i), layer_blocks, stride)
            self.layers.append(res_block)

        final_out_channels = 64 * (2 ** (len(desired_layers) - 1))
        fc_input_size = final_out_channels

        if laplacian_encoder_cls is not None:
            self.laplacian_encoder = self.laplacian_encoder_cls(out_channels=final_out_channels)

        if fourier_encoder_cls is not None:
            self.fourier_encoder = self.fourier_encoder_cls()

        if positional_encoding_cls is not None:
            self.positional_encoder = positional_encoding_cls(num_channels=final_out_channels)

        if attention_pooling_cls is not None:
            self.attention_pooling = self.attention_pooling_cls(in_channels=final_out_channels)
        elif self.use_global_pooling:
            self.global_pool = nn.AdaptiveAvgPool2d(1)

        self.fc = nn.Linear(fc_input_size, self.num_classes)

        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.Adam(self.parameters(), lr=self.learning_rate, weight_decay=self.weight_decay)
        self.scheduler = ReduceLROnPlateau(self.optimizer, mode='min', factor=0.5, patience=4, verbose=True)

    def make_layer(self, block, out_channels, blocks, stride):
        downsample = None
        if stride != 1 or self.in_channels != out_channels:
            downsample = nn.Sequential(
                nn.Conv2d(self.in_channels, out_channels, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(out_channels),
            )
        layer = []
        layer.append(block(self.in_channels, out_channels, stride, downsample))
        self.in_channels = out_channels
        for _ in range(1, blocks):
            layer.append(block(self.in_channels, out_channels, stride=1))
        return nn.Sequential(*layer)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        for layer in self.layers:
            x = layer(x)

        if hasattr(self, 'fourier_encoder'):
            fourier_encoding = self.fourier_encoder(x)
            x = x + fourier_encoding

        if hasattr(self, 'laplacian_encoder'):
            x = x + self.laplacian_encoder(x)

        if hasattr(self, 'positional_encoder'):
            x = self.positional_encoder(x)

        if hasattr(self, 'attention_pooling'):
            x = self.attention_pooling(x)
        elif self.use_global_pooling:
            x = self.global_pool(x)
        
        x = torch.flatten(x, 1)
        x = self.fc(x)

        return x

    def fit(self, train_loader, val_loader):
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.to(device)
        self.criterion = nn.CrossEntropyLoss()

        best_val_loss = float('inf')
        best_val_accuracy = 0.0
        best_model_state = None
        patience_counter = 0

        training_results = {'train_accuracy': [], 'train_loss': [], 'val_accuracy': [], 'val_loss': []}

        for epoch in range(self.epochs):
            self.train()
            total_loss = 0
            correct_predictions = 0
            total_samples = 0

            for inputs, labels in train_loader:
                inputs, labels = inputs.to(device), labels.to(device)

                self.optimizer.zero_grad()
                outputs = self(inputs)
                loss = self.criterion(outputs, labels)

                loss.backward()

                if self.max_grad_norm is not None:
                    torch.nn.utils.clip_grad_norm_(self.parameters(), self.max_grad_norm)

                self.optimizer.step()

                total_loss += loss.item()
                _, predicted = torch.max(outputs, 1)
                correct_predictions += (predicted == labels).sum().item()
                total_samples += labels.size(0)

            average_loss = total_loss / len(train_loader)
            accuracy = correct_predictions / total_samples * 100.0
            training_results['train_accuracy'].append(accuracy)
            training_results['train_loss'].append(average_loss)

            self.eval()
            val_loss = 0
            val_correct_predictions = 0
            val_total_samples = 0

            with torch.no_grad():
                for inputs, labels in val_loader:
                    inputs, labels = inputs.to(device), labels.to(device)
                    outputs = self(inputs)
                    val_loss += self.criterion(outputs, labels).item()
                    _, predicted = torch.max(outputs, 1)
                    val_correct_predictions += (predicted == labels).sum().item()
                    val_total_samples += labels.size(0)

            val_average_loss = val_loss / len(val_loader)
            val_accuracy = val_correct_predictions / val_total_samples * 100.0
            training_results['val_accuracy'].append(val_accuracy)
            training_results['val_loss'].append(val_average_loss)

            print(f"Epoch {epoch + 1}/{self.epochs}, Train Loss: {average_loss:.4f}, Train Accuracy: {accuracy:.2f}%, "
                f"Val Loss: {val_average_loss:.4f}, Val Accuracy: {val_accuracy:.2f}%")

            self.scheduler.step(val_average_loss)

            if val_accuracy > best_val_accuracy:
                best_val_accuracy = val_accuracy
                best_model_state = self.state_dict()

            if val_average_loss < best_val_loss:
                best_val_loss = val_average_loss
                patience_counter = 0
            else:
                patience_counter += 1

            if patience_counter >= self.patience_stop:
                print(f"Early stopping at epoch {epoch + 1} as validation loss didn't improve for {self.patience_stop} epochs.")
                break

        if best_model_state is not None:
            self.load_state_dict(best_model_state)

        return training_results

    def predict(self, test_loader):
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.to(device)
        self.eval()
        
        predictions = []
        with torch.no_grad():
            for inputs in test_loader:
                inputs = inputs.to(device)
                outputs = self(inputs)
                _, predicted = torch.max(outputs, 1)
                predictions.extend(predicted.cpu().numpy())
        
        return predictions
